import { customElement, html, LitElement, property } from "lit-element";

const STATE_HIDDEN = "hidden";
const STATE_SHOW = "show";
const STATE_INPUT = "input";

@customElement("matternelle-element")
export class MatternelleElement extends LitElement {
  private socket: WebSocket | null = null;
  private state: "hidden" | "show" | "input" = STATE_HIDDEN;
  private msgToSend: string = "";
  private msg: Array<any> = [];

  @property({ type: String, reflect: true })
  user: string = "";

  @property({ type: String, reflect: true, attribute: "token" })
  tokenApp: string = "";

  @property({ type: String, reflect: true, attribute: "url" })
  serverUrl: string = "";

  @property({ type: String, reflect: true, attribute: "placeholder" })
  placeholder: string = "";

  @property({ type: String, reflect: true, attribute: "default-msg" })
  defaultMsg: string = "";

  constructor() {
    super();
    this.initWS();
  }

  initWS() {
    this.msgToSend = "";
    this.msg = [];
    this.socket = new WebSocket(`ws://${this.serverUrl}/ws`);
    this.socket.onerror = error => {
      console.error(error);
    };

    this.socket.onopen = () => {
      console.log("Connection established.");
      if (this.tokenApp) {
        this.socket.send(
          JSON.stringify({ command: "tokenApp", appUserToken: this.tokenApp })
        );
      }
    };

    this.socket.onclose = () => {
      console.log("Connection terminated.");
      setTimeout(() => {
        this.initWS();
      }, 3000);
    };

    this.socket.onmessage = event => {
      const msg = JSON.parse(event.data);
      console.log("Message:", msg);
      if (
        msg.command === "nbChatUser" &&
        msg.nbChatUser !== undefined &&
        msg.nbChatUser > 0
      ) {
        this.state = STATE_SHOW;
        if (this.user) {
          this.socket.send(JSON.stringify({ 
            command: "msg", 
            msg: "new user: " + this.user 
          }));
        }
      }
      this.msg = [...this.msg, msg];
      this.requestUpdate();
    };
  }

  attributeChangedCallback(name, oldval, newval) {
    console.log("attribute change: ", name, newval);
    if (name === "user" && this.state !== STATE_HIDDEN) {
      this.socket.send(JSON.stringify({ command: "msg", msg: this.user }));
    } else if (name === "token" && this.state !== STATE_HIDDEN) {
      JSON.stringify({ command: "tokenApp", tokenApp: this.tokenApp });
    }
    super.attributeChangedCallback(name, oldval, newval);
  }

  start() {
    this.state = STATE_INPUT;
    this.requestUpdate();
  }

  handleClose() {
    this.state = STATE_SHOW;
    this.requestUpdate();
  }

  handleInput(e) {
    this.msgToSend = e.target.value;
    this.requestUpdate();
  }

  handleKeyPress(e) {
    if (e.target.value !== "") {
      if (e.key === "Enter") {
        this.sendMsg();
      }
    }
  }

  sendMsg() {
    const msg = { command: "msg", msg: this.msgToSend, byAppUser: true };
    this.msg = [...this.msg, msg];
    this.socket.send(JSON.stringify({ 
      command: "msg", 
      msg: this.user + ": " + this.msgToSend 
    }));
    this.msgToSend = "";
    this.requestUpdate();
  }

  protected render() {
    const msgTemplates = this.msg
      .filter(i => i.command === "msg")
      .map(
        i =>
          html`
            <li class="${i.byAppUser ? "byAppUser" : ""}">
              <div class="h-chat ${i.byAppUser ? "radius-right": "radius-left"}">
                ${i.msg}
              </div>
            </li>
          `
      );
    if (this.state === STATE_HIDDEN) {
      return html``;
    } else if (this.state === STATE_SHOW) {
      return html`
        <style>
          :host {
            display: block;
            position: fixed;
            bottom: 20%;
            right: 20px;
          }
          :host([hidden]) {
            display: none;
          }
          .btn {
            background-color: #4bda4b;
            color: #d460db;
            border-radius: 50%;
            width: 100px;
            height: 100px;
            border: none;
            font-weight: bold;
            font-size: 1.2em;
            cursor: pointer;
          }
        </style>

        <button class="btn" @click=${this.start}>Live Chat</button>
      `;
    }
    return html`
      <style>
        :host {
          display: block;
          width: 250px;
          height: 350px;
          position: fixed;
          bottom: 20%;
          right: 20px;
          background-color: #fefefe;
          box-shadow: 0 -1px 20px rgb(0,0,0,0.1);
          list-style: none;
          font-size: 0.8rem;
          font-weight: bold;
          font-family: quattrocento sans,sans-serif;
        }
        :host([hidden]) {
          display: none;
        }
        .closeBtn {
          position: absolute;
          top: -15px;
          right: -15px;
          background-color: #fefefe;
          border: 1px solid #ddd;
          border-radius: 50%;
          box-shadow: 0 -1px 20px rgb(0,0,0,0.1);
          width: 30px;
          height: 30px;
          cursor: pointer;
          transition: all .3s ease-in 0s;
        }
        .closeBtn:hover {
          background-color: #cdcdcd;
          border: 1px solid #bbb;
        }
        input {
          background-color: #eee;
          border: none;
          padding: 10px;
          width: calc( 100% - 20px );
          position: absolute;
          bottom: 0;
        }
        input:focus {
          outline: none;
        }
        .byAppUser {
          text-align: right;
        }
        .h-chat {
          background-color: #eee;
          display: inline-block;
          padding: 5px 15px;
          margin: 5px 20px;
          word-break: break-word;
          overflow-wrap: break-word;
        }
        .radius-left {
          border-radius: 0px 10px 10px 10px;
        }
        .radius-right {
          border-radius: 10px 0px 10px 10px;
        }
      </style>

      <button class="closeBtn" @click=${this.handleClose}>x</button>

      <ul class="listMsg">
        <li>
          ${this.defaultMsg}
        </li>
        ${msgTemplates}
      </ul>

      <input
        type="text"
        .value=${this.msgToSend}
        placeholder="${this.placeholder}"
        @input=${this.handleInput}
        @keypress=${this.handleKeyPress}
      />
      <button @click=${this.sendMsg}>+</button>
    `;
  }
}
